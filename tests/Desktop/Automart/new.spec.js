import { test, expect } from "@playwright/test";

test("open sidebar ", async ({ page }) => {
  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //Click Sidebar
  await page.locator('[aria-label="Open drawer"]').click();

  //Click Sidebar close button
  const closeSidebar = page.locator('[id="__next"] > div > div > .icon-ripple');

  //Assertion
  //Expected Result: Should redirect to login page
  await expect(closeSidebar).toBeVisible();
});
