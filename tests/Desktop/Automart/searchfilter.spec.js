import { test, expect } from '@playwright/test'


test("Open search filter dropdown ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.locator('#search-filter').click();

  // assertion
  //Expected Result: Should be able to show dropdown search filter
  const dropdownsearch = page.locator('#search-filter')
  await expect(dropdownsearch).toBeVisible();
});



test("search car by popular searches ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click search dropdown
  await page.locator('#search-filter').click();

  //Click Popular searches Mirage
  await page.locator('button:has-text("Mirage")').click();

  //Assertion
  //Expected Result: Should redirect to Mirage page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/search?q=Mirage');
});



test("search car by location", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //Location tab
  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-location > .MuiBadge-root > .MuiSvgIcon-root')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-location > .MuiBadge-root > .MuiSvgIcon-root')

  //Quezon City
  await page.waitForSelector('.MuiList-root > div > #list-item-3 > #checkbox-list-secondary-label-quezon-city > .MuiTypography-root')
  await page.click('.MuiList-root > div > #list-item-3 > #checkbox-list-secondary-label-quezon-city > .MuiTypography-root')

  //kalayaan
  await page.waitForSelector('.MuiList-root > .MuiListItem-container > #list-item-97 > #checkbox-list-label-97 > .MuiTypography-root')
  await page.click('.MuiList-root > .MuiListItem-container > #list-item-97 > #checkbox-list-label-97 > .MuiTypography-root')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')

  // assertion
  const searchlocation = await page.locator('#filter-submit').count()


  expect(searchlocation).toBeDefined()
});


test("search car by car type ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //car type
  await page.waitForSelector('#filter-car-type')
  await page.click('#filter-car-type')

  //SUV
  await page.waitForSelector('div:nth-child(4) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('div:nth-child(4) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const cartype = await page.locator('#filter-submit').count()


  expect(cartype).toBeDefined()
});


test("search car by brand ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //car brand
  await page.waitForSelector('#filter-brand')
  await page.click('#filter-brand')

  await page.waitForSelector('div:nth-child(2) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('div:nth-child(2) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')


  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const carbrand = await page.locator('#filter-submit').count()



  expect(carbrand).toBeDefined()
});

test("search car by Price ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //Price
  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-price > .MuiBadge-root > .mdi-icon')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-price > .MuiBadge-root > .mdi-icon')

  await page.waitForSelector('#simple-tabpanel-3 > .MuiBox-root > .MuiBox-root > #price-range-1m-below > .MuiChip-label')
  await page.click('#simple-tabpanel-3 > .MuiBox-root > .MuiBox-root > #price-range-1m-below > .MuiChip-label')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const carprice = await page.locator('#filter-submit').count()


  expect(carprice).toBeDefined()
});


test("search car by Year Model ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //year model
  await page.waitForSelector('#filter-year-model')
  await page.click('#filter-year-model')

  await page.waitForSelector('#simple-tabpanel-4 > .MuiBox-root > .MuiBox-root > #price-range-1m-below > .MuiChip-label')
  await page.click('#simple-tabpanel-4 > .MuiBox-root > .MuiBox-root > #price-range-1m-below > .MuiChip-label')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const yearmodel = await page.locator('#filter-submit').count()


  expect(yearmodel).toBeDefined()
});

test("search car by Transmission ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //transmission
  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-transmission > .MuiBadge-root > .mdi-icon')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-transmission > .MuiBadge-root > .mdi-icon')

  await page.waitForSelector('div:nth-child(1) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('div:nth-child(1) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const transmission = await page.locator('#filter-submit').count()


  expect(transmission).toBeDefined()
});

test("search car by Sale Type ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //Sale Type
  await page.waitForSelector('.MuiTabs-flexContainer > #filter-sale-type > .MuiBadge-root > .MuiSvgIcon-root > path')
  await page.click('.MuiTabs-flexContainer > #filter-sale-type > .MuiBadge-root > .MuiSvgIcon-root > path')

  await page.waitForSelector('div:nth-child(2) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('div:nth-child(2) > .MuiListItem-container > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const salestype = await page.locator('#filter-submit').count()


  expect(salestype).toBeDefined()

});


test("search car by Odometer ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");


  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //odometer
  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-odometer > .MuiBadge-root > .MuiSvgIcon-root')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-odometer > .MuiBadge-root > .MuiSvgIcon-root')

  await page.waitForSelector('#simple-tabpanel-7 > .MuiBox-root > .MuiBox-root > #odometer-range-10k-below > .MuiChip-label')
  await page.click('#simple-tabpanel-7 > .MuiBox-root > .MuiBox-root > #odometer-range-10k-below > .MuiChip-label')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const odometer = await page.locator('#filter-submit').count()


  expect(odometer).toBeDefined()
});

test("search car by Plate Ending ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");


  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //Plate ending

  await page.waitForSelector('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-plate-ending > .MuiBadge-root > .MuiSvgIcon-root')
  await page.click('.MuiTabs-scroller > .MuiTabs-flexContainer > #filter-plate-ending > .MuiBadge-root > .MuiSvgIcon-root')

  await page.waitForSelector('.MuiList-root > .MuiListItem-container:nth-child(1) > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')
  await page.click('.MuiList-root > .MuiListItem-container:nth-child(1) > .MuiListItemSecondaryAction-root > .MuiButtonBase-root > .PrivateSwitchBase-input')

  await page.waitForSelector('#filter-submit')
  await page.click('#filter-submit')
  // assertion
  const plateending = await page.locator('#filter-submit').count()


  expect(plateending).toBeDefined()
});

test("Reset Search ", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");


  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //search dropdown
  await page.waitForSelector('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')
  await page.click('.MuiBox-root > .MuiBox-root > #search-bar > #filter-icon-navbar > .MuiSvgIcon-root')

  //reset button
  await page.waitForSelector('#filter-reset')
  await page.click('#filter-reset')
  // assertion
  const resetbutton = await page.locator('#filter-reset').count()

  expect(resetbutton).toBeDefined()
}); 