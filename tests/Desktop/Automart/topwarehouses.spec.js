import { test, expect } from '@playwright/test'


test("View Our Locations Top Warehouses", async ({ page }) => {

    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    // assertion
    const Topwarehouses = await page.locator('#locations-tab-warehouses').count()
  
    // verify assertion
    expect(Topwarehouses).toBeDefined()
  }); 
  
  test("Select General Trias Cavite Top Warehouses should redirect to Gen Trias Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Gen Trias
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy')
  
    // assertion
    const GenTrias = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy').count()
  
    // verify assertion
    expect(GenTrias).toBeDefined()
  }); 

  test("Select Panabo Top Warehouses should redirect to Panabo Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Panabo
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-panabo > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-panabo > .css-r02kiy')
  
    // assertion
    const panabo = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-panabo > .css-r02kiy').count()
  
    // verify assertion
    expect(panabo).toBeDefined()
  }); 

  test("Select Santo Tomas Top Warehouses should redirect to Santo Tomas Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Santo Tomas
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-santo-tomas > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-santo-tomas > .css-r02kiy')
  
    // assertion
    const ST = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-panabo > .css-r02kiy').count()
  
    // verify assertion
    expect(ST).toBeDefined()
  }); 
  
  test("Select Guiguinto Top Warehouses should redirect to Guiguinto Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Guiguinto
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-guiguinto > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-guiguinto > .css-r02kiy')
  
    // assertion
    const guiguinto = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-panabo > .css-r02kiy').count()
  
    // verify assertion
    expect(guiguinto).toBeDefined()
  }); 

  test("Select Cabuyao Top Warehouses should redirect to Cabuyao Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Cabuyao
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-cabuyao > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-cabuyao > .css-r02kiy')
  
    // assertion
    const cabuyao = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-cabuyao > .css-r02kiy').count()
  
    // verify assertion
    expect(cabuyao).toBeDefined()
  }); 

  test("Select Ayala Malls Top Warehouses should redirect to Ayala Malls Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Ayala Malls
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-ayala-malls > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-ayala-malls > .css-r02kiy')
  
    // assertion
    const AM = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-ayala-malls > .css-r02kiy').count()
  
    // verify assertion
    expect(AM).toBeDefined()
  }); 

  test("Select Singcang Top Warehouses should redirect to Singcang Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Singcang
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-singcang > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-singcang > .css-r02kiy')
  
    // assertion
    const singcang = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-singcang > .css-r02kiy').count()
  
    // verify assertion
    expect(singcang).toBeDefined()
  }); 

  test("Select Panacan Top Warehouses should redirect to Panacan Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Panacan
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-panacan > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-panacan > .css-r02kiy')
  
    // assertion
    const panacan = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-panacan > .css-r02kiy').count()
  
    // verify assertion
    expect(panacan).toBeDefined()
  }); 

  test("Select Banaybanay Top Warehouses should redirect to Banaybanay Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Banay banay
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-banay-banay > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-banay-banay > .css-r02kiy')
  
    // assertion
    const bb = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-banay-banay > .css-r02kiy').count()
  
    // verify assertion
    expect(bb).toBeDefined()
  }); 

  test("Select Carmelite Rd Top Warehouses should redirect to Carmelite Rd Page", async ({ page }) => {
  
    // Go to the Automart V5 carkuya page web page
    await page.goto("https://automart-staging-v5.vercel.app/");
  
    // Wait for the page to load
    await page.waitForLoadState('networkidle');
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')
  
    //Carmelite Rd
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-carmelite-rd > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-carmelite-rd > .css-r02kiy')
  
    // assertion
    const CR = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-carmelite-rd > .css-r02kiy').count()
  
    // verify assertion
    expect(CR).toBeDefined()
  }); 