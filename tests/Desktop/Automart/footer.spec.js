import { test, expect } from '@playwright/test'
//var randomstring = require("randomstring")


test("Click Certified Cars in the Footer should redirect to certified cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  // Click Certified Cars Footer
  await page.locator('p:has-text("Certified Cars")').click();

  //Assertion
  //Expected Result: Should redirect to certified cars page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/certified-cars');
});

test("Click Used Cars for Sale in the Footer should redirect to All Used cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //click Used Cars for sale Footer
  await page.locator('text=Used Cars for Sale').click();

  //Assertion
  //Expected Results: Should redirect to All used cars page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/all');

});

test("Click Repossessed Cars in the Footer should redirect to All Used cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Repossessed Cars Footer
  await page.locator('text=Repossessed Cars for Sale').click();
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/all');
});


//---------------------------------------- SERVICES---------------------------------------------------------

test("Click Sell My Car in the Footer should redirect to Sell or Trade-in cars page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click sell my car Cars footer
  await page.locator('p:has-text("Sell my Car")').click();

  //Assertion
  //Expected Result: Should redirect to Sell my car page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/sell-my-car');
});

test("Click Car Insurance in the Footer should redirect to Assurance.ph page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Car Insurance Footer
  await page.locator('p:has-text("Car Insurance")').click();

  //Assertion
  //Expected Result: Should redirect to assurance page
  await expect(page).toHaveURL('https://assurance.ph/');

});



//---------------------------------------- PARTNERS---------------------------------------------------------



test("Click Eastwest Bank in Footer should redirect to Eastwest Bank page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Eastwest bank Footer
  await page.locator('text=Eastwest Bank').click();

  //Assertion
  //Expected Result: Should redirect to eastwest bank page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/repossessed-cars/eastwest-bank');
});

test("Click BDO in Footer should redirect to BDO Bank page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click BDO bank Footer
  await page.locator('#footer-list >> text=BDO').click();

  //Assertion
  //Expected Result: Should redirect to BDO Page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/repossessed-cars/bdo');
});

test("Click Security Bank in Footer should redirect to Security Bank page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Security bank Footer
  await page.locator('text=Security Bank').click();

  //Assertion
  //Expect Result: Should redirect to Security bank page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/repossessed-cars/security-bank');
});

test("Click Orix in Footer should redirect to Orix page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Orix page
  await page.locator('text=Orix').click();

  //Assertion
  //Expected Result: Should redirect to Orix Page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/repossessed-cars/orix');
});


test("Click TFS in Footer should redirect to TFS page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click TFS Footer
  await page.locator('text=TFS').click();

  //Assertion
  //Expected Result: Should redirect TFS page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/repossessed-cars/tfs');
});



test("Click Buena Mano in Footer should redirect to Buena Mano page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Buena Mano Footer  
  await page.locator('text=Buena Mano').click();

  //Assertion
  //Expected Result: Should redirect to Buena Mano Page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/repossessed-cars/buena-mano');
});



test("Click Yulon Finance Philippines Corp in Footer should redirect to Yulon Finance Philippines Corp page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Yulon Finance Philippines Corp Footer  
  await page.locator('text=Yulon Finance Philippines Corp').click();

  //Assertion
  //Expected Result: Should redirect to Yulon Finance Philippines Corp Page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/repossessed-cars/yulon_finance_philippines_corp');
});



//---------------------------------------- OTHERS ---------------------------------------------------------

test("Click Blog in Footer should redirect to Blog page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Blog Footer  
  await page.locator('p:has-text("Blog")').click();

  //Assertion
  //Expected Result: Should redirect to Blog Page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/blog');
});


test("Click Contact Us in Footer should redirect to Contact Us page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Contact Us Footer  
  await page.locator('p:has-text("Contact Us")').click();

  //Assertion
  //Expected Result: Should redirect to Contact Us Page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/contact-us');
});




test("Click Assetmart.Global in Footer should redirect to Assetmart.Global page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Assetmart.Global Footer  
  await page.locator('text=Assetmart.Global').click();

  //Assertion
  //Expected Result: Should redirect to Assetmart.Global Page
  await expect(page).toHaveURL('https://assetmart.global/');
});


test("Click Assurance.Ph in Footer should redirect to Assurance.Ph page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Assurance.Ph Footer  
  await page.locator('#footer-list >> text=Assurance.Ph').click();

  //Assertion
  //Expected Result: Should redirect to Assurance.Ph Page
  await expect(page).toHaveURL('https://assurance.ph/');
});



//---------------------------------------- SUPPORT TOOLS---------------------------------------------------------

test("Click Installment Calculator in Footer should redirect to Installment Calculator page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Installment Calculator Footer  
  await page.locator('p:has-text("Installment Calculator")').click();

  //Assertion
  //Expected Result: Should redirect to Installment Calculator Page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/installment-calculator');
});


test("Click FAQs in Footer should redirect to FAQs page", async ({ page }) => {

  // Go to the Automart V5 carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click FAQs Footer  
  await page.locator('p:has-text("FAQs")').click();

  //Assertion
  //Expected Result: Should redirect to FAQs Page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/blog/frequently-asked-questions');
});

