import { test, expect } from '@playwright/test'


test("Enter valid Email Address & valid Password", async ({ page }) => {

  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Email Address
  await page.locator('[placeholder="Email Address \\*"]').click();

  //Enter Email Address
  await page.locator('[placeholder="Email Address \\*"]').fill('testv5@gmail.com');

  //Click Pasword textbox
  await page.locator('[placeholder="Password \\*"]').click();

  //Enter Password
  await page.locator('[placeholder="Password \\*"]').fill('Superpw64');

  //Click Submit button
  await page.locator('text=SUBMIT').click();

  //Assertion
  //Expected Result: Should redirect to Home page
  await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/');
});


test("Enter Invalid Email Address & valid Password", async ({ page }) => {

  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Email Address textbox
  await page.locator('[placeholder="Email Address \\*"]').click();

  //Enter Email Address
  await page.locator('[placeholder="Email Address \\*"]').fill('testsample@gmail.com');


  //Click Pasword textbox
  await page.locator('[placeholder="Password \\*"]').click();

  //Enter Password
  await page.locator('[placeholder="Password \\*"]').fill('qwe');

  //Click Submit button
  await page.locator('text=SUBMIT').click();

  //Assertion
  //Expected Result: Should be able to show Error Message "INVALID CREDENTIALS"
  const errorMessage = page.locator('text=Invalid credentials')
  await expect(errorMessage).toContainText('Invalid credentials')
});



test("Enter Valid Email Address & Invalid Password", async ({ page }) => {

  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Email Address textbox
  await page.locator('[placeholder="Email Address \\*"]').click();

  //Enter Email Address
  await page.locator('[placeholder="Email Address \\*"]').fill('testsample@gmail.com');


  //Click Pasword textbox
  await page.locator('[placeholder="Password \\*"]').click();

  //Enter Password
  await page.locator('[placeholder="Password \\*"]').fill('qwe');

  //Click Submit button
  await page.locator('text=SUBMIT').click();

  //Assertion
  //Expected Result: Should be able to show Error Message "INVALID CREDENTIALS"
  const errorMessage = page.locator('text=Invalid credentials')
  await expect(errorMessage).toContainText('Invalid credentials')
});



test("Enter Invalid Email Address & Invalid Password", async ({ page }) => {

  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Email Address textbox
  await page.locator('[placeholder="Email Address \\*"]').click();

  //Enter Email Address
  await page.locator('[placeholder="Email Address \\*"]').fill('testsample@gmail.com');


  //Click Pasword textbox
  await page.locator('[placeholder="Password \\*"]').click();

  //Enter Password
  await page.locator('[placeholder="Password \\*"]').fill('qwe');

  //Click Submit button
  await page.locator('text=SUBMIT').click();

  //Assertion
  //Expected Result: Should be able to show Error Message "INVALID CREDENTIALS"
  const errorMessage = page.locator('text=Invalid credentials')
  await expect(errorMessage).toContainText('Invalid credentials')
});

test("Enter Null Email Address & Null Password", async ({ page }) => {

  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Submit button
  await page.locator('text=SUBMIT').click();

  //Assertion
  //Expected Result: Should be able to show Error Message "INVALID CREDENTIALS"
  const errorMessage = page.locator('text=email format is invalid ,email should not be empty,password should not be empty')
  await expect(errorMessage).toContainText('email format is invalid ,email should not be empty,password should not be empty')
});

test("Enter Null Email Address & Valid Password", async ({ page }) => {

  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click Password Textbox
  await page.locator('[placeholder="Password \\*"]').click();

  //Enter Password
  await page.locator('[placeholder="Password \\*"]').fill('qwe')

  //Click Submit button
  await page.locator('text=SUBMIT').click();

  //Assertion
  //Expected Result: Should be able to show Error Message "Email format is invalid"
  const errorMessage = page.locator('text=email format is invalid ,email should not be empty')
  await expect(errorMessage).toContainText('email format is invalid ,email should not be empty')
});


test("Enter Valid Email Address & Null Password", async ({ page }) => {

  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState('networkidle');

  //Click email address textbox
  await page.locator('[placeholder="Email Address \\*"]').click();

  //Enter Email address
  await page.locator('[placeholder="Email Address \\*"]').fill('testv5@gmail.com');

  //Click Submit button
  await page.locator('text=SUBMIT').click();

  //Assertion
  //Expected Result: Should be able to show Error Message "Email format is invalid"
  const errorMessage = page.locator('text=password should not be empty')
  await expect(errorMessage).toContainText('password should not be empty')
});

test("Login using facebook", async ({ page }) => {
  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  // Click Facebook
  await page.click("#facebook-login-button");

  //Assertion
  //Expected Result: Should be able to click login button and show pop up page
  const facebook = page.locator("#facebook-login-button");
  await expect(facebook).toHaveText("Facebook");
});

test("Login using Google Account", async ({ page }) => {
  // Go to the Assetmart carkuya page web page
  await page.goto("https://automart-staging-v5.vercel.app/login");

  // Wait for the page to load
  await page.waitForLoadState("networkidle");

  //click google button
  await page.click("#google-login-button");

  //Assertion
  //Expected Result: Should be able to click login button and show pop up page
  const google = page.locator("#google-login-button");
  await expect(google).toHaveText("Google");
});