import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'


test("View Automart.ph Homepage", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Homepage
    await page.locator('text=+63 927 887 6400Certified CarsUsed CarsSell My CarInstallment CalculatorReviewsB >> img[alt="Automart\\.Ph"]').click();

    //assertion
    await expect(page).toHaveURL('https://automart-staging-v5.vercel.app/');

  /*  // assertion
    const Homepage = await page.locator('.MuiContainer-root > div > .MuiBox-root:nth-child(1) > .MuiContainer-root > .MuiTypography-root').count()

    // verify assertion
    expect(Homepage).toBeDefined()
    */
  }
  });
test("Click Watchlist Heart button", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Heart button
    await page.waitForSelector('.MuiToolbar-root > .css-18j51vt > #favorite-button-navbar > .MuiSvgIcon-root > path')
    await page.click('.MuiToolbar-root > .css-18j51vt > #favorite-button-navbar > .MuiSvgIcon-root > path')

    // assertion
    const Heartbutton = await page.locator('.MuiToolbar-root > .css-18j51vt > #favorite-button-navbar > .MuiSvgIcon-root > path').count()

    // verify assertion
    expect(Heartbutton).toBeDefined()
  }
});

test(" Click Contact Number in Navbar", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Contact number
    await page.waitForSelector('.MuiPaper-root > .MuiToolbar-root > .css-18j51vt > #telephone-button-navbar > .MuiTypography-root')
    await page.click('.MuiPaper-root > .MuiToolbar-root > .css-18j51vt > #telephone-button-navbar > .MuiTypography-root')

    // assertion
    const telephonenavbar = await page.locator('.MuiPaper-root > .MuiToolbar-root > .css-18j51vt > #telephone-button-navbar > .MuiTypography-root').count()

    // verify assertion
    expect(telephonenavbar).toBeDefined()
  }
});

test(" Click Certified Cars in Navbar should redirect to certified used cars page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Certified Cars
    await page.waitForSelector('.MuiToolbar-root > .MuiBox-root > #certified-cars-subnavbar > svg > path:nth-child(1)')
    await page.click('.MuiToolbar-root > .MuiBox-root > #certified-cars-subnavbar > svg > path:nth-child(1)')

    // assertion
    const certfiedcarsnavbar = await page.locator('.MuiToolbar-root > .MuiBox-root > #certified-cars-subnavbar > svg > path:nth-child(1)').count()

    // verify assertion
    expect(certfiedcarsnavbar).toBeDefined()
  }
});

test(" Click Used Cars in Navbar should redirect to All used cars page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Used Cars
    await page.waitForSelector('.MuiCollapse-wrapperInner > .MuiToolbar-root > .MuiBox-root > #used-cars-subnavbar > svg')
    await page.click('.MuiCollapse-wrapperInner > .MuiToolbar-root > .MuiBox-root > #used-cars-subnavbar > svg')

    // assertion
    const usedcarsnavbar = await page.locator('.MuiCollapse-wrapperInner > .MuiToolbar-root > .MuiBox-root > #used-cars-subnavbar > svg').count()

    // verify assertion
    expect(usedcarsnavbar).toBeDefined()
  }
});

test(" Click Sell My Car in Navbar should redirect to Sell My Car Page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());



    //Sell My Car
    await page.waitForSelector('#sell-my-car-subnavbar')
    await page.click('#sell-my-car-subnavbar')

    // assertion
    const sellmycarnavbar = await page.locator('#sell-my-car-subnavbar').count()

    // verify assertion
    expect(sellmycarnavbar).toBeDefined()
  }
});

test(" Click Reviews in Navbar should redirect to Reviews page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //reviews
    await page.waitForSelector('#reviews-subnavbar')
    await page.click('#reviews-subnavbar')
    // assertion
    const reviewsnavbar = await page.locator('#reviews-subnavbar').count()

    // verify assertion
    expect(reviewsnavbar).toBeDefined()
  }
});

test(" Click Blogs in Navbar should redirect to Blogs page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Blogs
    await page.waitForSelector('.MuiCollapse-wrapperInner > .MuiToolbar-root > .MuiBox-root > #blogs-subnavbar > svg')
    await page.click('.MuiCollapse-wrapperInner > .MuiToolbar-root > .MuiBox-root > #blogs-subnavbar > svg')

    // assertion
    const blogsnavbar = await page.locator('.MuiCollapse-wrapperInner > .MuiToolbar-root > .MuiBox-root > #blogs-subnavbar > svg').count()

    // verify assertion
    expect(blogsnavbar).toBeDefined()
  }
});

test("Click Buy button in Homepage should redirect to All used cars page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Buy button
    await page.waitForSelector('#hero-button-buy')
    await page.click('#hero-button-buy')

    // assertion
    const Buybutton = await page.locator('#hero-button-buy').count()

    // verify assertion
    expect(Buybutton).toBeDefined()
  }
});

test("Click Sell or Trade-In button in Homepage should redirect to Sell My Car Page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Sell or Trade-In button
    await page.waitForSelector('#hero-button-sell')
    await page.click('#hero-button-sell')

    // assertion
    const Tradeinbutton = await page.locator('#hero-button-sell').count()

    // verify assertion
    expect(Tradeinbutton).toBeDefined()
  }
});

test("Click Insurance button in Homepage should redirect to Assurance Page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Insurance button
    await page.waitForSelector('#hero-button-insurance')
    await page.click('#hero-button-insurance')
    // assertion
    const Insurancebutton = await page.locator('#hero-button-insurance').count()

    // verify assertion
    expect(Insurancebutton).toBeDefined()
  }
  });

test("Click Browse all Cars button should redirect to All used cars page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Insurance button
    await page.waitForSelector('#hero-button-browse')
    await page.click('#hero-button-browse')
    // assertion
    const Browseallcars = await page.locator('#hero-button-browse').count()

    // verify assertion
    expect(Browseallcars).toBeDefined()
  }
  });

test("View Automart Certified vehicle", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //view automart certified vehicle
    await page.waitForSelector('.MuiContainer-root > div > div:nth-child(2) > span > img')
    await page.click('.MuiContainer-root > div > div:nth-child(2) > span > img')
    // assertion
    const viewACUV = await page.locator('.MuiContainer-root > div > div:nth-child(2) > span > img').count()

    // verify assertion
    expect(viewACUV).toBeDefined()
  }
  });

test("Click See All ACV button should redirect to Certified Cars page ", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //view automart certified vehicle
    await page.waitForSelector('#button-see-all-acv')
    await page.click('#button-see-all-acv')
    // assertion
    const allACVbutton = await page.locator('#button-see-all-acv').count()

    // verify assertion
    expect(allACVbutton).toBeDefined()
  }
  });

test("Contact Us using viber", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //contact us using viber
    await page.waitForSelector('#contact-us-1')
    await page.click('#contact-us-1')


    // assertion
    const viber = await page.locator('#contact-us-1').count()

    // verify assertion
    expect(viber).toBeDefined()
  }
  });

test("Contact Us using telegram", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //contact us using telegram
    await page.waitForSelector('#contact-us-2')
    await page.click('#contact-us-2')


    // assertion
    const telegram = await page.locator('#contact-us-2').count()

    // verify assertion
    expect(telegram).toBeDefined()
  }
  });

test("Contact Us using email", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //contact us using email
    await page.waitForSelector('#contact-us-3')
    await page.click('#contact-us-3')


    // assertion
    const email = await page.locator('#contact-us-3').count()

    // verify assertion
    expect(email).toBeDefined()
  }
  });

test("Contact Us using messenger", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //contact us using messenger
    await page.waitForSelector('#contact-us-4')
    await page.click('#contact-us-4')


    // assertion
    const messenger = await page.locator('#contact-us-4').count()

    // verify assertion
    expect(messenger).toBeDefined()
  }
  });

test("View Popular Vehicles by Model", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Popular vehicles by
    await page.waitForSelector('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')

    //Model
    await page.waitForSelector('#tab-popular-vehicles-by-model')
    await page.click('#tab-popular-vehicles-by-model')

    // assertion
    const model = await page.locator('#tab-popular-vehicles-by-model').count()

    // verify assertion
    expect(model).toBeDefined()
  }
  });

test("Select Toyota Model in Popular vehicles should redirect to Toyota Model Page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Popular vehicles by
    await page.waitForSelector('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')

    //Model
    await page.waitForSelector('#tab-popular-vehicles-by-model')
    await page.click('#tab-popular-vehicles-by-model')

    //Select vios
    await page.waitForSelector('.MuiGrid-root > .MuiGrid-root > #lp-card-toyota-vios > .MuiBox-root > .MuiTypography-root:nth-child(1)')
    await page.click('.MuiGrid-root > .MuiGrid-root > #lp-card-toyota-vios > .MuiBox-root > .MuiTypography-root:nth-child(1)')

    // assertion
    const toyotaModel = await page.locator('.MuiGrid-root > .MuiGrid-root > #lp-card-toyota-vios > .MuiBox-root > .MuiTypography-root:nth-child(1)').count()

    // verify assertion
    expect(toyotaModel).toBeDefined()
  }
  });

test("Click See All Car Models button should redirect to All Models page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Popular vehicles by
    await page.waitForSelector('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')

    //Model
    await page.waitForSelector('#tab-popular-vehicles-by-model')
    await page.click('#tab-popular-vehicles-by-model')

    //see all car model button
    await page.waitForSelector('#popular-see-all-models-button')
    await page.click('#popular-see-all-models-button')

    // assertion
    const SeeAllCarModel = await page.locator('#popular-see-all-models-button').count()

    // verify assertion
    expect(SeeAllCarModel).toBeDefined()
  }
  });

test("View Popular Vehicles by Body Type", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Popular vehicles by
    await page.waitForSelector('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')

    //Body type
    await page.waitForSelector('#tab-popular-vehicles-by-body-type')
    await page.click('#tab-popular-vehicles-by-body-type')

    // assertion
    const bodytype = await page.locator('#tab-popular-vehicles-by-body-type').count()

    // verify assertion
    expect(bodytype).toBeDefined()
  }
  });

test("Select Sedan Body Type should redirect to Sedan page(should show all the sedan type vehicles) ", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Popular vehicles by
    await page.waitForSelector('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')

    //Body type
    await page.waitForSelector('#tab-popular-vehicles-by-body-type')
    await page.click('#tab-popular-vehicles-by-body-type')

    //sedan
    await page.waitForSelector('.MuiGrid-root > .MuiGrid-root > #lp-card-sedan > .MuiBox-root > .MuiTypography-root:nth-child(1)')
    await page.click('.MuiGrid-root > .MuiGrid-root > #lp-card-sedan > .MuiBox-root > .MuiTypography-root:nth-child(1)')

    // assertion
    const sedanbodyType = await page.locator('.MuiGrid-root > .MuiGrid-root > #lp-card-sedan > .MuiBox-root > .MuiTypography-root:nth-child(1)').count()

    // verify assertion
    expect(sedanbodyType).toBeDefined()
  }
  });

test("View Popular Vehicles by Brand", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Popular vehicles by
    await page.waitForSelector('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')

    //Brand
    await page.waitForSelector('#tab-popular-vehicles-by-brand')
    await page.click('#tab-popular-vehicles-by-brand')

    // assertion
    const brand = await page.locator('#tab-popular-vehicles-by-brand').count()

    // verify assertion
    expect(brand).toBeDefined()
  }
  });

test("Select Toyota Brand Should redirect to Toyota Page ", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Popular vehicles by
    await page.waitForSelector('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')

    //Brand
    await page.waitForSelector('#tab-popular-vehicles-by-brand')
    await page.click('#tab-popular-vehicles-by-brand')

    //Toyota
    await page.waitForSelector('.MuiGrid-root > .MuiGrid-root > #lp-card-toyota > .MuiBox-root > .MuiTypography-root:nth-child(1)')
    await page.click('.MuiGrid-root > .MuiGrid-root > #lp-card-toyota > .MuiBox-root > .MuiTypography-root:nth-child(1)')

    // assertion
    const ToyotaBrand = await page.locator('.MuiGrid-root > .MuiGrid-root > #lp-card-toyota > .MuiBox-root > .MuiTypography-root:nth-child(1)').count()

    // verify assertion
    expect(ToyotaBrand).toBeDefined()
  }
  });

test("Click See All Car Brands button should redirect to All Brands page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Popular vehicles by
    await page.waitForSelector('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .css-1qsxih2 > .MuiTypography-root')

    //Brand
    await page.waitForSelector('#tab-popular-vehicles-by-brand')
    await page.click('#tab-popular-vehicles-by-brand')

    //see all car brands button
    await page.waitForSelector('#popular-see-all-brands-button')
    await page.click('#popular-see-all-brands-button')

    // assertion
    const SeeAllCarBrand = await page.locator('#popular-see-all-brands-button').count()

    // verify assertion
    expect(SeeAllCarBrand).toBeDefined()
  }
  });

test("3 simple steps to get your car -> find the right car", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //find the right car
    await page.waitForSelector('#simple-steps-browse-all-cars')
    await page.click('#simple-steps-browse-all-cars')


    // assertion
    const findrightcar = await page.locator('#simple-steps-browse-all-cars').count()

    // verify assertion
    expect(findrightcar).toBeDefined()
  }
  });

test("3 simple steps to get your car -> connect to an adviser", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //connecdt to an  adviser
    await page.waitForSelector('#simple-steps-connect-to-an-adviser')
    await page.click('#simple-steps-connect-to-an-adviser')


    // assertion
    const connectadviser = await page.locator('#simple-steps-connect-to-an-adviser').count()

    // verify assertion
    expect(connectadviser).toBeDefined()
  }
});



test("3 simple steps to get your car -> Buy immediately or bid on a car", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Buy immediately or bid on a car
    await page.waitForSelector('#simple-steps-learn-more')
    await page.click('#simple-steps-learn-more')

    // assertion
    const learnmore = await page.locator('#simple-steps-learn-more').count()

    // verify assertion
    expect(learnmore).toBeDefined()
  }
});

test("Sell / Trade-in your car", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //get quotation button
    await page.waitForSelector('#can-do-get-quotation')
    await page.click('#can-do-get-quotation')


    // assertion
    const getquotationbutton = await page.locator('#can-do-get-quotation').count()

    // verify assertion
    expect(getquotationbutton).toBeDefined()
  }
});

test("Get car insurance", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //connect to an adviser button
    await page.waitForSelector('#can-do-connect-to-an-adviser')
    await page.click('#can-do-connect-to-an-adviser')


    // assertion
    const connecttoanadviserbutton = await page.locator('#can-do-connect-to-an-adviser').count()

    // verify assertion
    expect(connecttoanadviserbutton).toBeDefined()
  }
});

test("View FAQs click expand button", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //view faqs dropdown
    await page.waitForSelector('.MuiGrid-root > #faq-accordion-0 > #panel0-header > .MuiAccordionSummary-expandIconWrapper > .MuiSvgIcon-root')
    await page.click('.MuiGrid-root > #faq-accordion-0 > #panel0-header > .MuiAccordionSummary-expandIconWrapper > .MuiSvgIcon-root')


    // assertion
    const faqsdropdown = await page.locator('.MuiGrid-root > #faq-accordion-0 > #panel0-header > .MuiAccordionSummary-expandIconWrapper > .MuiSvgIcon-root').count()

    // verify assertion
    expect(faqsdropdown).toBeDefined()
  }
});

test("View more questions button", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //view more question button
    await page.waitForSelector('.MuiContainer-root > div > .MuiContainer-root > .MuiBox-root > .MuiButtonBase-root')
    await page.click('.MuiContainer-root > div > .MuiContainer-root > .MuiBox-root > .MuiButtonBase-root')

    // assertion
    const viewmorequestionbutton = await page.locator('.MuiContainer-root > div > .MuiContainer-root > .MuiBox-root > .MuiButtonBase-root').count()

    // verify assertion
    expect(viewmorequestionbutton).toBeDefined()
  }
});

test("View All Cars", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //view All Cars
    await page.waitForSelector('#__next > .MuiContainer-root > div > .item-listing-preview > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .item-listing-preview > .MuiTypography-root')

    // assertion
    const viewAllCars = await page.locator('#__next > .MuiContainer-root > div > .item-listing-preview > .MuiTypography-root').count()

    // verify assertion
    expect(viewAllCars).toBeDefined()
  }
});

test("Click See All Used Cars button should redirect to All Used Cars Page ", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //view All Cars
    await page.waitForSelector('#__next > .MuiContainer-root > div > .item-listing-preview > .MuiTypography-root')
    await page.click('#__next > .MuiContainer-root > div > .item-listing-preview > .MuiTypography-root')

    // see all used cars button
    await page.waitForSelector('#button-see-all-used-cars')
    await page.click('#button-see-all-used-cars')

    // assertion
    const AllUsedCarsButton = await page.locator('#button-see-all-used-cars').count()

    // verify assertion
    expect(AllUsedCarsButton).toBeDefined()
  }
});


test("View Our Locations Top Warehouses", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')

    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')

    // assertion
    const Topwarehouses = await page.locator('#locations-tab-warehouses').count()

    // verify assertion
    expect(Topwarehouses).toBeDefined()
  }
});


test("Select General Trias Cavite Top Warehouses should redirect to Gen Trias Page", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());

    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')

    //Top Warehouses
    await page.waitForSelector('#locations-tab-warehouses')
    await page.click('#locations-tab-warehouses')

    //Gen Trias
    await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy')
    await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy')

    // assertion
    const GenTrias = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy').count()

    // verify assertion
    expect(GenTrias).toBeDefined()
  }
});

test("View Our Locations Top Cities", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/' });
    console.log(await page.title());


    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')

    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')

    // assertion
    const Topcities = await page.locator('#locations-tab-cities').count()

    // verify assertion
    expect(Topcities).toBeDefined()
  }
});

