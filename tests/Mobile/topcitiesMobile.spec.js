
import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'


test("View Our Locations Top Cities", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //Our Locations
  await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  
  //Top Cities
  await page.waitForSelector('#locations-tab-cities')
  await page.click('#locations-tab-cities')

  // assertion
  const Topcities = await page.locator('#locations-tab-cities').count()

  // verify assertion
  expect(Topcities).toBeDefined()
}); 

test("Select Cavite Top cities should redirect to Cavite Page", async ({}) => {

  // Go to the Automart V5 carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //Our Locations
  await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  
  //Top Cities
  await page.waitForSelector('#locations-tab-cities')
  await page.click('#locations-tab-cities')

  //Cavite
  await page.waitForSelector('#top-cities-cavite')
  await page.click('#top-cities-cavite')

  // assertion
  const Cavite = await page.locator('#top-cities-cavite').count()

  // verify assertion
  expect(Cavite).toBeDefined()
}); 

test("Select Davao Top Warehouses should redirect to Davao Page", async ({}) => {

 // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //Davao
    await page.waitForSelector('#top-cities-davao')
    await page.click('#top-cities-davao')
  
    // assertion
    const davao = await page.locator('#top-cities-davao').count()
  
    // verify assertion
    expect(davao).toBeDefined()
}); 

test("Select Bulacan Top cities should redirect to Bulacan Page", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //Bulacan
    await page.waitForSelector('#top-cities-bulacan')
    await page.click('#top-cities-bulacan')
  
    // assertion
    const Bulacan = await page.locator('#top-cities-bulacan').count()
  
    // verify assertion
    expect(Bulacan).toBeDefined()
}); 

test("Select Batangas Top cities should redirect to Batangas Page", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //Batangas
    await page.waitForSelector('#top-cities-batangas')
    await page.click('#top-cities-batangas')
  
    // assertion
    const Batangas = await page.locator('#top-cities-batangas').count()
  
    // verify assertion
    expect(Batangas).toBeDefined()
}); 

test("Select Cabuyao Top cities should redirect to Cabuyao Page", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //Cabuyao
    await page.waitForSelector('#top-cities-cabuyao')
    await page.click('#top-cities-cabuyao')
  
    // assertion
    const Cabuyao = await page.locator('#top-cities-cabuyao').count()
  
    // verify assertion
    expect(Cabuyao).toBeDefined()
}); 

test("Select Cebu Top cities should redirect to Cebu Page", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //Cebu
    await page.waitForSelector('#top-cities-cebu')
    await page.click('#top-cities-cebu')
  
    // assertion
    const Cebu = await page.locator('#top-cities-cebu').count()
  
    // verify assertion
    expect(Cebu).toBeDefined()
}); 

test("Select Parañaque Top cities should redirect to Parañaque Page", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //Paranaque
    await page.waitForSelector('#top-cities-paranaque')
    await page.click('#top-cities-paranaque')
  
    // assertion
    const Paranaque = await page.locator('#top-cities-paranaque').count()
  
    // verify assertion
    expect(Paranaque).toBeDefined()
}); 

test("Select Quezon City Top cities should redirect to Quezon City Page", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //QC
    await page.waitForSelector('#top-cities-quezon-city')
    await page.click('#top-cities-quezon-city')
  
    // assertion
    const QC = await page.locator('#top-cities-quezon-city').count()
  
    // verify assertion
    expect(QC).toBeDefined()
}); 

test("Select Bacolod Top cities should redirect to Bacolod Page", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //Bacolod
    await page.waitForSelector('#top-cities-bacolod')
    await page.click('#top-cities-bacolod')
  
    // assertion
    const Bacolod = await page.locator('#top-cities-bacolod').count()
  
    // verify assertion
    expect(Bacolod).toBeDefined()
}); 

test("Select Cagayan de Oro Top cities should redirect to Cagayan de Oro Page", async ({}) => {

    // Go to the Automart V5 carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    
    
    //Our Locations
    await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
    
    //Top Cities
    await page.waitForSelector('#locations-tab-cities')
    await page.click('#locations-tab-cities')
  
    //Cagayan de Oro
    await page.waitForSelector('#top-cities-cagayan-de-oro')
    await page.click('#top-cities-cagayan-de-oro')
  
    // assertion
    const Cagayan = await page.locator('#top-cities-cagayan-de-oro').count()
  
    // verify assertion
    expect(Cagayan).toBeDefined()
}); 
