
import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'

test("Enter valid Email Address & valid Password", async  ({}) => {

  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  
    // Take screenshot of the whole device.
    await device.screenshot({ path: 'device.png' });

  {
     // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto('https://automart-staging-v5.vercel.app/');

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
    await page.waitForSelector('#login-email')
    await page.click('#login-email')
    await page.type('#login-email','jammel.qmarketz@gmail.com')
    
    await page.waitForSelector('#login-password')
    await page.click('#login-password')
    await page.type('#login-password','Superpw64')

    await page.waitForSelector('#login-button')
    await page.click('#login-button')

    // assertion
    const Successlogin = await page.locator('.MuiToolbar-root > .css-dhs4d9 > .jss1 > span > img').count()
  
    // verify if go to signup modal
    expect(Successlogin).toBeDefined()
    console.log(Successlogin)
  }
}); 


