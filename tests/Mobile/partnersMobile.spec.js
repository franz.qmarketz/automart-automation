
import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'


test("Eastwest Bank view page", async ({}) => {

    // Go to the automart carkuya page web page
    
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));
  
   
    

    await page.waitForSelector('#login-email')
    await page.click('#login-email')
    await page.type('#login-email','jammel.qmarketz@gmail.com')
    
    await page.waitForSelector('#login-password')
    await page.click('#login-password')
    await page.type('#login-password','Superpw64')

    await page.waitForSelector('#login-button')
    await page.click('#login-button')

    await page.waitForSelector('#footer-link-eastwest-bank')
    await page.click('#footer-link-eastwest-bank')

    //eastwest page
    await page.waitForSelector('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')
    await page.click('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')

    // assertion
    const eastwestpage = await page.locator('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root').count()
  
    // verify if go to signup modal
    expect(eastwestpage).toBeDefined()
  }); 


  
test("Click Browse all cars from Eastwest Bank", async ({}) => {

  // Go to the automart carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  await page.waitForSelector('#login-email')
  await page.click('#login-email')
  await page.type('#login-email','jammel.qmarketz@gmail.com')
  
  await page.waitForSelector('#login-password')
  await page.click('#login-password')
  await page.type('#login-password','Superpw64')

  await page.waitForSelector('#login-button')
  await page.click('#login-button')

  await page.waitForSelector('#footer-link-eastwest-bank')
  await page.click('#footer-link-eastwest-bank')

  //eastwest page
  await page.waitForSelector('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')

  //browse button
  await page.waitForSelector('.MuiContainer-root > .main > div > .MuiContainer-root > .MuiButtonBase-root')
  await page.click('.MuiContainer-root > .main > div > .MuiContainer-root > .MuiButtonBase-root')

  // assertion
  const EWbrowseallbutton = await page.locator('.MuiContainer-root > .main > div > .MuiContainer-root > .MuiButtonBase-root').count()

  // verify if go to signup modal
  expect(EWbrowseallbutton).toBeDefined()
}); 



test("Contact automart ph using viber", async ({}) => {

  // Go to the automart carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  await page.waitForSelector('#login-email')
  await page.click('#login-email')
  await page.type('#login-email','jammel.qmarketz@gmail.com')
  
  await page.waitForSelector('#login-password')
  await page.click('#login-password')
  await page.type('#login-password','Superpw64')

  await page.waitForSelector('#login-button')
  await page.click('#login-button')

  await page.waitForSelector('#footer-link-eastwest-bank')
  await page.click('#footer-link-eastwest-bank')

  //eastwest page
  await page.waitForSelector('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')

  //viber button
  await page.waitForSelector('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(2) > .MuiTypography-root')
  await page.click('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(2) > .MuiTypography-root')

  // assertion
  const viberbutton = await page.locator('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(2) > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(viberbutton).toBeDefined()
}); 



test("Eastwest Bank  Contact automart ph using telephone", async ({}) => {

  // Go to the automart carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  await page.waitForSelector('#login-email')
  await page.click('#login-email')
  await page.type('#login-email','jammel.qmarketz@gmail.com')
  
  await page.waitForSelector('#login-password')
  await page.click('#login-password')
  await page.type('#login-password','Superpw64')

  await page.waitForSelector('#login-button')
  await page.click('#login-button')

  await page.waitForSelector('#footer-link-eastwest-bank')
  await page.click('#footer-link-eastwest-bank')

  //eastwest page
  await page.waitForSelector('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')

  //telephone button
  await page.waitForSelector('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(3) > .MuiTypography-root')
  await page.click('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(3) > .MuiTypography-root')

  // assertion
  const telephonebutton = await page.locator('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(3) > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(telephonebutton).toBeDefined()
}); 


test("Eastwest Bank Contact automart ph using email", async ({}) => {

  // Go to the automart carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  await page.waitForSelector('#login-email')
  await page.click('#login-email')
  await page.type('#login-email','jammel.qmarketz@gmail.com')
  
  await page.waitForSelector('#login-password')
  await page.click('#login-password')
  await page.type('#login-password','Superpw64')

  await page.waitForSelector('#login-button')
  await page.click('#login-button')

  await page.waitForSelector('#footer-link-eastwest-bank')
  await page.click('#footer-link-eastwest-bank')

  //eastwest page
  await page.waitForSelector('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')

  //email
  await page.waitForSelector('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(4) > .MuiTypography-root')
  await page.click('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(4) > .MuiTypography-root')

  // assertion
  const email = await page.locator('div > .MuiContainer-root > .css-1vk1r5w-contactsContainer > .css-1jfdya2-contact:nth-child(4) > .MuiTypography-root').count()

  // verify if go to signup modal
  expect(email).toBeDefined()
}); 




test("Eastwest Bank click Get an offer button should redirect to sell my car page", async ({}) => {

  // Go to the automart carkuya page web page
  
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  

  await page.waitForSelector('#login-email')
  await page.click('#login-email')
  await page.type('#login-email','jammel.qmarketz@gmail.com')
  
  await page.waitForSelector('#login-password')
  await page.click('#login-password')
  await page.type('#login-password','Superpw64')

  await page.waitForSelector('#login-button')
  await page.click('#login-button')

  await page.waitForSelector('#footer-link-eastwest-bank')
  await page.click('#footer-link-eastwest-bank')

  //eastwest page
  await page.waitForSelector('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('.main > div > .css-gzbt0w-root > .MuiContainer-root > .MuiTypography-root')

  //get an offer
  await page.waitForSelector('.main > div > .MuiContainer-root > .MuiPaper-root > .MuiButtonBase-root')
  await page.click('.main > div > .MuiContainer-root > .MuiPaper-root > .MuiButtonBase-root')

  // assertion
  const getanoffer = await page.locator('.main > div > .MuiContainer-root > .MuiPaper-root > .MuiButtonBase-root').count()

  // verify if go to signup modal
  expect(getanoffer).toBeDefined()
}); 


test("View Our Locations Top Warehouses", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //Our Locations
  await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  
  //Top Warehouses
  await page.waitForSelector('#locations-tab-warehouses')
  await page.click('#locations-tab-warehouses')

  // assertion
  const Topwarehouses = await page.locator('#locations-tab-warehouses').count()

  // verify assertion
  expect(Topwarehouses).toBeDefined()
}); 


test("Select General Trias Cavite Top Warehouses should redirect to Gen Trias Page", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //Our Locations
  await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  
  //Top Warehouses
  await page.waitForSelector('#locations-tab-warehouses')
  await page.click('#locations-tab-warehouses')

  //Gen Trias
  await page.waitForSelector('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy')
  await page.click('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy')

  // assertion
  const GenTrias = await page.locator('#locations-tabpanel-0 > .MuiGrid-root > .MuiGrid-root > #top-warehouses-gen-trias > .css-r02kiy').count()

  // verify assertion
  expect(GenTrias).toBeDefined()
}); 

test("View Our Locations Top Cities", async ({}) => {

  // Go to the Automart V5 carkuya page web page

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.

  // Launch Chrome browser.
  await device.shell('am force-stop com.android.chrome');
  const context = await device.launchBrowser();
  // Use BrowserContext as usual.
  const page = await context.newPage();
  // Go to the Automart carkuya page web page
  await page.goto(endpoint.AUTOMART_URL);

 
  

  await page.evaluate(() => window.scrollTo(0, document.body.scrollHeight));

 
  
  
  //Our Locations
  await page.waitForSelector('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  await page.click('body > #__next > .MuiContainer-root > .MuiContainer-root > .MuiTypography-root')
  
  //Top Cities
  await page.waitForSelector('#locations-tab-cities')
  await page.click('#locations-tab-cities')

  // assertion
  const Topcities = await page.locator('#locations-tab-cities').count()

  // verify assertion
  expect(Topcities).toBeDefined()
}); 
