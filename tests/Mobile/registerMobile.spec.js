import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'
var randomstring = require("randomstring")


test("Register without email address", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());

    //Click Password textbox
    await page.locator('[placeholder="Password"]').click();

    //Enter Password
    await page.locator('[placeholder="Password"]').fill("Superpw64");

    //Click Confirm password textbox
    await page.locator('[placeholder="Confirm Password"]').click();

    //Enter confirm password
    await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

    //Click First name textbox
    await page.locator('[placeholder="First Name"]').click();

    //Enter First name
    await page.locator('[placeholder="First Name"]').fill("Jammel");

    //Click Middle name textbox
    await page.locator('[placeholder="Middle Name"]').click();

    //Enter Middle name
    await page.locator('[placeholder="Middle Name"]').fill("test");

    //Click Last name textbox
    await page.locator('[placeholder="Last Name"]').click();

    //Enter Lastname
    await page.locator('[placeholder="Last Name"]').fill("Account");

    //Click Contact number textbox
    await page.locator('[placeholder="Contact Number"]').click();

    //Enter Contact number
    await page.locator('[placeholder="Contact Number"]').fill("0912346485");

    //Click Sign up button
    await page.click("#sign-up-registration-button");

    //Assertion
    //Expected Result: Should be able to see error message
    const noemail = page.locator("text=EmailThis field is required");
    await expect(noemail).toHaveText("EmailThis field is required");
  }
});


test("Register without Password", async ({ }) => {


  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());

   //Click email address textbox
   await page.locator('[placeholder="Email"]').click();

   //enter email address with Randomstring
   await page.locator('[placeholder="Email"]').fill(
     randomstring.generate({
       length: 7,
       charset: "alphabetic",
     }) + "@gmail.com"
   );
 
   //Click Confirm password textbox
   await page.locator('[placeholder="Confirm Password"]').click();
 
   //Enter confirm password
   await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");
 
   //Click First name textbox
   await page.locator('[placeholder="First Name"]').click();
 
   //Enter First name
   await page.locator('[placeholder="First Name"]').fill("Jammel");
 
   //Click Middle name textbox
   await page.locator('[placeholder="Middle Name"]').click();
 
   //Enter Middle name
   await page.locator('[placeholder="Middle Name"]').fill("test");
 
   //Click Last name textbox
   await page.locator('[placeholder="Last Name"]').click();
 
   //Enter Lastname
   await page.locator('[placeholder="Last Name"]').fill("Account");
 
   //Click Contact number textbox
   await page.locator('[placeholder="Contact Number"]').click();
 
   //Enter Contact number
   await page.locator('[placeholder="Contact Number"]').fill("0912346485");
 
   //Click Sign up button
   await page.click("#sign-up-registration-button");
 
   //Assertion
   //Expected Result: Should be able to see error message that password field is required
   const noPassword = page.locator("text=PasswordThis field is required");
   await expect(noPassword).toHaveText("PasswordThis field is required");
  }
});


test("Register without confirm Password", async ({ }) => {


  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());

   //Click email address textbox
   await page.locator('[placeholder="Email"]').click();

   //enter email address with Randomstring
   await page.locator('[placeholder="Email"]').fill(
     randomstring.generate({
       length: 7,
       charset: "alphabetic",
     }) + "@gmail.com"
   );
 
   //Click Password textbox
   await page.locator('[placeholder="Password"]').click();
 
   //Enter Password
   await page.locator('[placeholder="Password"]').fill("Superpw64");
 
   //Click First name textbox
   await page.locator('[placeholder="First Name"]').click();
 
   //Enter First name
   await page.locator('[placeholder="First Name"]').fill("Jammel");
 
   //Click Middle name textbox
   await page.locator('[placeholder="Middle Name"]').click();
 
   //Enter Middle name
   await page.locator('[placeholder="Middle Name"]').fill("test");
 
   //Click Last name textbox
   await page.locator('[placeholder="Last Name"]').click();
 
   //Enter Lastname
   await page.locator('[placeholder="Last Name"]').fill("Account");
 
   //Click Contact number textbox
   await page.locator('[placeholder="Contact Number"]').click();
 
   //Enter Contact number
   await page.locator('[placeholder="Contact Number"]').fill("0912346485");
 
   //Click Sign up button
   await page.click("#sign-up-registration-button");
 
   //Assertion
   //Expected Result: Should be able to see Error message that confirm password is required
   const noConfirmPassword = page.locator(
     "text=Confirm PasswordThis field is required"
   );
   await expect(noConfirmPassword).toHaveText(
     "Confirm PasswordThis field is required"
   );
  }
});


test("Register without first name", async ({ }) => {


  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());

    //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see Error message that First name is required
  const noFirstName = page.locator("text=First NameThis field is required");
  await expect(noFirstName).toHaveText("First NameThis field is required");
  }
});


test("Register without middle name", async ({ }) => {


  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());

 //Click email address textbox
 await page.locator('[placeholder="Email"]').click();

 //enter email address with Randomstring
 await page.locator('[placeholder="Email"]').fill(
   randomstring.generate({
     length: 7,
     charset: "alphabetic",
   }) + "@gmail.com"
 );

 //Click Password textbox
 await page.locator('[placeholder="Password"]').click();

 //Enter Password
 await page.locator('[placeholder="Password"]').fill("Superpw64");

 //Click Confirm password textbox
 await page.locator('[placeholder="Confirm Password"]').click();

 //Enter confirm password
 await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

 //Click First name textbox
 await page.locator('[placeholder="First Name"]').click();

 //Enter First name
 await page.locator('[placeholder="First Name"]').fill("Jammel");

 //Click Last name textbox
 await page.locator('[placeholder="Last Name"]').click();

 //Enter Lastname
 await page.locator('[placeholder="Last Name"]').fill("Account");

 //Click Contact number textbox
 await page.locator('[placeholder="Contact Number"]').click();

 //Enter Contact number
 await page.locator('[placeholder="Contact Number"]').fill("0912346485");

 //Click Sign up button
 await page.click("#sign-up-registration-button");

 //Assertion
 //Expected Result: Should be able to see error message that required Middle name or type N/A
 const noMiddleName = page.locator(
   'xpath=//*[@id="registration-form"]/div/div[4]/div[2]/p[2]'
 );
 await expect(noMiddleName).toBeVisible();
  }
});


test("Register without last name", async ({ }) => {


  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());

    //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Contact number textbox
  await page.locator('[placeholder="Contact Number"]').click();

  //Enter Contact number
  await page.locator('[placeholder="Contact Number"]').fill("0912346485");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see Error message that required last name
  const noLastName = page.locator("text=Last NameThis field is required");
  await expect(noLastName).toHaveText("Last NameThis field is required");
  }
});



test("Register without contact number", async ({ }) => {


  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());

   //Click email address textbox
  await page.locator('[placeholder="Email"]').click();

  //enter email address with Randomstring
  await page.locator('[placeholder="Email"]').fill(
    randomstring.generate({
      length: 7,
      charset: "alphabetic",
    }) + "@gmail.com"
  );

  //Click Password textbox
  await page.locator('[placeholder="Password"]').click();

  //Enter Password
  await page.locator('[placeholder="Password"]').fill("Superpw64");

  //Click Confirm password textbox
  await page.locator('[placeholder="Confirm Password"]').click();

  //Enter confirm password
  await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

  //Click First name textbox
  await page.locator('[placeholder="First Name"]').click();

  //Enter First name
  await page.locator('[placeholder="First Name"]').fill("Jammel");

  //Click Middle name textbox
  await page.locator('[placeholder="Middle Name"]').click();

  //Enter Middle name
  await page.locator('[placeholder="Middle Name"]').fill("test");

  //Click Last name textbox
  await page.locator('[placeholder="Last Name"]').click();

  //Enter Lastname
  await page.locator('[placeholder="Last Name"]').fill("Account");

  //Click Sign up button
  await page.click("#sign-up-registration-button");

  //Assertion
  //Expected Result: Should be able to see error message that require contact number
  const noContactNumber = page.locator("text=Contact NumberThis field is required");
  await expect(noContactNumber).toHaveText("Contact NumberThis field is required");
  }
});

test("Register Successfully", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());

//Click email address textbox
await page.locator('[placeholder="Email"]').click();

//enter email address with Randomstring
await page.locator('[placeholder="Email"]').fill(
  randomstring.generate({
    length: 7,
    charset: "alphabetic",
  }) + "@gmail.com"
);

//Click Password textbox
await page.locator('[placeholder="Password"]').click();

//Enter Password
await page.locator('[placeholder="Password"]').fill("Superpw64");

//Click Confirm password textbox
await page.locator('[placeholder="Confirm Password"]').click();

//Enter confirm password
await page.locator('[placeholder="Confirm Password"]').fill("Superpw64");

//Click First name textbox
await page.locator('[placeholder="First Name"]').click();

//Enter First name
await page.locator('[placeholder="First Name"]').fill("Jammel");

//Click Middle name textbox
await page.locator('[placeholder="Middle Name"]').click();

//Enter Middle name
await page.locator('[placeholder="Middle Name"]').fill("test");

//Click Last name textbox
await page.locator('[placeholder="Last Name"]').click();

//Enter Lastname
await page.locator('[placeholder="Last Name"]').fill("Account");

//Click Contact number textbox
await page.locator('[placeholder="Contact Number"]').click();

//Enter Contact number
await page.locator('[placeholder="Contact Number"]').fill("0912346485");

//Click Sign up button
await page.click("#sign-up-registration-button");

//Assertion
//Expected Result: Should be able to click signup button and redirect to welcome page
const signup = page.locator("#sign-up-registration-button");
await expect(signup).toHaveText("SIGN UP");
  }
});



test("Register Successfully using facebook", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());


    await page.waitForSelector('#facebook-login-button')
    await page.click('#facebook-login-button')

    // assertion
    const RegisterFacebook = await page.locator('#facebook-login-button').count()

    // verify if go to signup modal
    expect(RegisterFacebook).toBeDefined()
  }
});



test("Register Successfully using Google", async ({ }) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);
  // Take screenshot of the device.


  // Take screenshot of the whole device.
  await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'https://automart-staging-v5.vercel.app/register');
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: 'https://automart-staging-v5.vercel.app/register' });
    console.log(await page.title());


    await page.waitForSelector('#google-login-button')
    await page.click('#google-login-button')

    // assertion
    const RegisterGoogle = await page.locator('#google-login-button').count()

    // verify if go to signup modal
    expect(RegisterGoogle).toBeDefined()
  }
}); 