
import { test, expect } from '@playwright/test'
import { _android as android, } from 'playwright'

test("Enter valid Email Address & valid Password", async  ({}) => {

  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);

    // Take screenshot of the whole device.
    await device.screenshot({ path: 'device.png' });

  {
    // --------------------- WebView -----------------------

    // Launch an application with WebView.
    await device.shell('am force-stop org.chromium.webview_shell');
    await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');
    // Get the WebView.
    const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });

    // Fill the input box.
    await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, "https://automart-staging-v5.vercel.app/");
    await device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter');

    // Work with WebView's page as usual.
    const page = await webview.page();
    await page.waitForNavigation({ url: "https://automart-staging-v5.vercel.app/" });
    console.log(await page.title());
    
//certified cars

await page.waitForSelector('.scrollbar-hide > #certified-cars-subnavbar > span > svg > path:nth-child(1)')
await page.click('.scrollbar-hide > #certified-cars-subnavbar > span > svg > path:nth-child(1)')
console.log(await page.click('.scrollbar-hide > #certified-cars-subnavbar > span > svg > path:nth-child(1)'))


// assertion
const CertifiedCars = await page.locator('.scrollbar-hide > #certified-cars-subnavbar > span > svg > path:nth-child(1)').count()
console.log(CertifiedCars)
// verify assertion
expect(CertifiedCars).toBeDefined()
  }
  }); 
