import { devices, PlaywrightTestConfig } from "@playwright/test";


const config: PlaywrightTestConfig = {
  use: {
    launchOptions: {
      args: ["--start-maximized"],
      slowMo: 10,
    },
    headless: true,
    viewport: null,
  },
  workers: 1,
  fullyParallel: true,
  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] },
    },
    //  {
    //       name: 'Pixel',
    //         use: {...devices['Pixel 5'] },
    //     },
    //         {
    //    name: 'Mobile Safari',
    //  use: 
    //   {...devices['iPhone 12'],
    //        },
    // {
    //      name: 'firefox',
    //     use: { ...devices['Desktop Firefox'] },
    //   },
    //    {
    //        name: 'webkit',
    //        use: { ...devices['Desktop Safari'] },
    //   },
  ],

}
export default config;
